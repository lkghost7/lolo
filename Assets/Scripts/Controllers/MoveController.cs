﻿using System;
using System.Collections;
using UnityEngine;

public class MoveController : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    [SerializeField] private Rigidbody2D _rb;
    [SerializeField] private float MAX_SPEED = 10;
    [SerializeField] private Transform heroTransform;
    
    [SerializeField] private Transform rayToGroundLeft;
    [SerializeField] private Transform rayToGroundRight;
    [SerializeField] private Transform rayToProbe;
    
    private bool isRun;
    private bool ladderStay;
    private PlayerState _playerState;
    
    void Update()
    {
        print(_animator.GetInteger("base") + " _animator");
        Vector2 velocity = _rb.velocity;
        IsGround();

   
        if (!IsGround() && _rb.velocity.y < 0 && !ladderStay)
        {
            _animator.SetInteger("base", 3);

        }
        
        if (ladderStay)
        {
            MoveToLadder();
        }
        
        if (!IsGround())
        {
            if (velocity.y <= -20f && -25f <= velocity.y)
            {
                _rb.velocity = Vector2.down * 40f;
            }
        }
        else
        {

            if (!ladderStay)
            {
                _animator.SetInteger("base", 1);
            }
        }
        

        if (!IsGround())
        {
            isRun = true;
            return;
        }
        
        if (!isRun)
        {
            _animator.SetInteger("base", 1);
        }
        
        MoveLeft();
        MoveRight();
    }
    
    public void MoveUp()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            heroTransform.localRotation = new Quaternion(0, 0, 0, 0);
            _rb.velocity = Vector2.up * MAX_SPEED;
        }
        
        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            _rb.velocity = Vector2.zero;
        }
    }
    
    public void MoveDown()
    {
        if (Input.GetKey(KeyCode.DownArrow))
        {
            heroTransform.localRotation = new Quaternion(0, 0, 0, 0);
            _rb.velocity = Vector2.down * MAX_SPEED;
        }
        
        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            _rb.velocity = Vector2.zero;
        }
    }
    
    public void MoveLeftLadder()
    {
        if (Input.GetKey(KeyCode.LeftArrow) )
        {
            _animator.SetInteger("base", 2);
            heroTransform.localRotation = new Quaternion(0, -90, 0, 90);
            _rb.velocity = new Vector2(-1f * MAX_SPEED, 0);
        }
        
        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            _rb.velocity = Vector2.zero;
        }
    }
    
    public void MoveRightLadder()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            _animator.SetInteger("base", 2);
            heroTransform.localRotation = new Quaternion(0, 90, 0, 90);
            _rb.velocity = new Vector2(1f * MAX_SPEED, 0);
        }
        
        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            _rb.velocity = Vector2.zero;
        }
    }
    
    public void MoveUpLadder(LadderController ladderController)
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            heroTransform.localRotation = new Quaternion(0, 0, 0, 0);
            _rb.velocity = Vector2.up * MAX_SPEED;

            if (ladderController != null)
            {
                Vector2 target = new Vector3(ladderController.transform.position.x + 0.5f, transform.position.y);
                transform.position = Vector2.MoveTowards(transform.position, target, 0.09f);
            }
        }
        
        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            _rb.velocity = Vector2.zero;
        }
    }
    
    public void MoveDownLadder(LadderController ladderController)
    {
        if (Input.GetKey(KeyCode.DownArrow))
        {
            heroTransform.localRotation = new Quaternion(0, 0, 0, 0);
            _rb.velocity = Vector2.down * MAX_SPEED;
            
            if (ladderController != null)
            {
                Vector2 target = new Vector3(ladderController.transform.position.x + 0.5f, transform.position.y);
                transform.position = Vector2.MoveTowards(transform.position, target, 0.09f);
            }
        }
        
        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            _rb.velocity = Vector2.zero;
        }
    }

    public void MoveLeft()
    {
        if (Input.GetKey(KeyCode.LeftArrow) && IsGround())
        {
            isRun = true;
          
            _animator.SetInteger("base", 2);
            heroTransform.localRotation = new Quaternion(0, -90, 0, 90);
            _rb.velocity = new Vector2(-1f * MAX_SPEED, 0);
        }
        
        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            isRun = false;
            _rb.velocity = Vector2.zero;
            _animator.SetInteger("base", 1);
        }
    }

    public void MoveRight()
    {
        if (Input.GetKey(KeyCode.RightArrow) && IsGround())
        {
            isRun = true;
            _animator.SetInteger("base", 2);
            
            heroTransform.localRotation = new Quaternion(0, 90, 0, 90);
            _rb.velocity = new Vector2(1f * MAX_SPEED, 0);
        }
        
        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            isRun = false;
            _rb.velocity = Vector2.zero;
            _animator.SetInteger("base", 1);
        }
    }
    
    public bool IsGround()
    {
        int layerMask = LayerMask.GetMask("Ground");
        Vector2 chekGroundPointLeft = rayToGroundLeft.position;
        Vector2 chekGroundPointRight = rayToGroundRight.position;
        
        chekGroundPointLeft.y -= 0.1f;
        chekGroundPointRight.y -= 0.1f;
        
        RaycastHit2D hitLeft = Physics2D.Raycast(chekGroundPointLeft, Vector2.down, 0.1f, layerMask);
        RaycastHit2D hitRight = Physics2D.Raycast(chekGroundPointRight, Vector2.down, 0.1f, layerMask);
        
       
        if (hitLeft.collider != null || hitRight.collider != null)
        {
            return true;
        }

        return false;
    }

    public void LadderStay()
    {
        _rb.gravityScale = 0;
        _animator.SetInteger("base", 4);
        ladderStay = true;
        _rb.velocity = Vector2.zero;
    }
    
    public void LadderOf()
    {
        _animator.SetInteger("base", 1);
        _rb.gravityScale = 10;
        ladderStay = false;
        _rb.velocity = Vector2.zero;
    }
    
    private void MoveToLadder()
    {
        LadderController ladderController = null;
        _rb.gravityScale = 0;

        int layerMask = LayerMask.GetMask("Ladder");
        Vector2 probe = rayToProbe.position;
        
        RaycastHit2D hitProbe = Physics2D.Raycast(probe, Vector2.right, 7f, layerMask);
        
        if (hitProbe.collider != null && hitProbe.collider.CompareTag("Ladder"))
        {
            ladderController = hitProbe.collider.GetComponent<LadderController>();
            print(ladderController.name + " ladderController");
        }
        
        MoveUpLadder(ladderController);
        MoveDownLadder(ladderController);
        MoveLeftLadder();
        MoveRightLadder();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Vector2 target = new Vector2(rayToProbe.transform.position.x +7f, rayToProbe.transform.position.y);
        Gizmos.DrawLine(rayToProbe.transform.position, target);
    }
}