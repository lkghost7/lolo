﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderController : MonoBehaviour
{
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            MoveController moveController = other.GetComponent<MoveController>();
            moveController.LadderStay();
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            MoveController moveController = other.GetComponent<MoveController>();
            moveController.LadderOf();
        }
    }
}